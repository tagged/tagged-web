package net.strainovic.tagged.taggedweb.controller;

import net.strainovic.tagged.taggedweb.dao.UserDao;
import net.strainovic.tagged.taggedweb.model.TokenIssuer;
import net.strainovic.tagged.taggedweb.model.User;
import net.strainovic.tagged.taggedweb.model.UserToken;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(UserInfoController.class)
public class UserInfoControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private UserDao userDao;

    @Test
    @WithMockUser(username = "mstrainovic")
    public void basePositiveDropboxAuthorisedTest() throws Exception {
        User mockedUser = new User();
        mockedUser.setUserId("mstrainovic");
        mockedUser.addUserToken(new UserToken(TokenIssuer.DROPBOX, UUID.randomUUID().toString(), "dropboxUserId"));

        when(userDao.findOne("mstrainovic")).thenReturn(mockedUser);

        mvc.perform(get("/v1/userInfo")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$..tokens[0].name").value("DROPBOX"))
                .andExpect(jsonPath("$..tokens[0].tokenIssuer").value("DROPBOX"))
                .andExpect(jsonPath("$..tokens[0].tokenIssuerId").value("dropboxUserId"))
                .andExpect(jsonPath("$..tokens[0].link").value("/dropbox"));


        verify(userDao, times(1)).findOne("mstrainovic");
    }

    @Test
    public void unauthorizedUserTest() throws Exception {
        mvc.perform(get("/v1/userInfo")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andExpect(status().isUnauthorized())
                .andExpect(status().reason("Full authentication is required to access this resource"));

    }

}