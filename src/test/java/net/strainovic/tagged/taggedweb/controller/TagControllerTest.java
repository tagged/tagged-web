package net.strainovic.tagged.taggedweb.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.strainovic.tagged.taggedweb.controller.command.AddTagCommand;
import net.strainovic.tagged.taggedweb.controller.command.EditTagCommand;
import net.strainovic.tagged.taggedweb.hystrix.command.TaggedRestAddTagsCommand;
import net.strainovic.tagged.taggedweb.hystrix.command.TaggedRestEditTagsCommand;
import net.strainovic.tagged.taggedweb.model.TokenIssuer;
import net.strainovic.tagged.taggedweb.service.TagService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;
import java.util.UUID;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(TagController.class)
public class TagControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private TagService tagService;

    @Test
    @WithMockUser(username = "mstrainovic")
    public void addTagsBasicPositiveTest() throws Exception {
        String externalId = UUID.randomUUID().toString();

        AddTagCommand addTagCommand = new AddTagCommand();
        addTagCommand.setFileId(externalId);
        addTagCommand.setName("File1");
        addTagCommand.setTags(Collections.singletonList("beach"));
        addTagCommand.setProvider(TokenIssuer.DROPBOX);
        addTagCommand.setProviderId("someRandomProviderId");

        mvc.perform(post("/v1/tags").contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(addTagCommand))).andExpect(status().is2xxSuccessful());

        ArgumentCaptor<TaggedRestAddTagsCommand> commandCaptor = ArgumentCaptor.forClass(TaggedRestAddTagsCommand.class);
        verify(tagService, times(1)).addTags(commandCaptor.capture());

        TaggedRestAddTagsCommand capturedValue = commandCaptor.getValue();

        Assert.assertEquals(externalId, capturedValue.getFileId());
        Assert.assertEquals("DROPBOX", capturedValue.getProvider());
        Assert.assertEquals("someRandomProviderId", capturedValue.getProviderId());
        Assert.assertEquals("mstrainovic", capturedValue.getUsername());
        Assert.assertEquals("File1", capturedValue.getName());
        Assert.assertEquals(Collections.singletonList("beach"), capturedValue.getTags());
    }

    @Test
    @WithMockUser(username = "mstrainovic")
    public void addTagsIdIsNotSendTest() throws Exception {
        AddTagCommand addTagCommand = new AddTagCommand();
        addTagCommand.setName("File1");
        addTagCommand.setTags(Collections.singletonList("beach"));
        addTagCommand.setProvider(TokenIssuer.DROPBOX);

        mvc.perform(post("/v1/tags").contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(addTagCommand)))
                .andExpect(status().is(400))
                .andExpect(status().isBadRequest());
    }

    /**
     * If tags are empty delete all tags for specific document
     */
    @Test
    @WithMockUser(username = "mstrainovic")
    public void deleteTagsBasicPositiveTest() throws Exception {
        String id = UUID.randomUUID().toString();

        mvc.perform(delete("/v1/tags/" + id).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful());

        verify(tagService, times(1)).deleteTags(id);
    }

    @Test
    @WithMockUser(username = "mstrainovic")
    public void editTagsBasicPositiveTest() throws Exception {
        String id = UUID.randomUUID().toString();

        EditTagCommand editTagCommand = new EditTagCommand();
        editTagCommand.setId(id);
        editTagCommand.setTags(Collections.singletonList("beach"));

        mvc.perform(put("/v1/tags").contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(editTagCommand))).andExpect(status().is2xxSuccessful());

        ArgumentCaptor<TaggedRestEditTagsCommand> commandCaptor = ArgumentCaptor.forClass(TaggedRestEditTagsCommand.class);
        verify(tagService, times(1)).editTags(commandCaptor.capture());

        TaggedRestEditTagsCommand capturedValue = commandCaptor.getValue();

        Assert.assertEquals(id, capturedValue.getId());
        Assert.assertEquals(Collections.singletonList("beach"), capturedValue.getTags());
    }

}