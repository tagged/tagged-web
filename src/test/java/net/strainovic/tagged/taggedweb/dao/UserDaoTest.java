package net.strainovic.tagged.taggedweb.dao;

import net.strainovic.tagged.taggedweb.model.TokenIssuer;
import net.strainovic.tagged.taggedweb.model.User;
import net.strainovic.tagged.taggedweb.model.UserToken;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

@RunWith(SpringRunner.class)
@DataJpaTest
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class UserDaoTest {

    @Autowired
    private UserDao userDao;

    //TODO reimplement test
    @Ignore
    @Test
    public void CRUDTest() {
        String randomUuid = UUID.randomUUID().toString();

        //Create
        User user = new User();
        user.setUserId("mstrainovic");
        user.addUserToken(new UserToken(TokenIssuer.DROPBOX, randomUuid, "someRandomId"));

        userDao.save(user);

        //Read
        User userFromDb = userDao.findOne("mstrainovic");
        Assert.assertEquals(1, userFromDb.getUserTokens().size());
        Optional<UserToken> token = userFromDb.findTokenByIssuer(TokenIssuer.DROPBOX);
        Assert.assertTrue(token.isPresent());
        Assert.assertEquals(randomUuid, token.get().getTokenValue());
        Assert.assertEquals("someRandomId", token.get().getTokenIssuerId());

        //Update token
        String newRandomUuid = UUID.randomUUID().toString();
        userFromDb.addUserToken(new UserToken(TokenIssuer.DROPBOX, newRandomUuid, "someRandomId"));
        userDao.save(userFromDb);

        userFromDb = userDao.findOne("mstrainovic");
        Assert.assertEquals(1, userFromDb.getUserTokens().size());
        token = userFromDb.findTokenByIssuer(TokenIssuer.DROPBOX);
        Assert.assertTrue(token.isPresent());
        Assert.assertEquals(newRandomUuid, token.get().getTokenValue());

        userFromDb.getUserTokens().clear();
        userDao.save(userFromDb);
        userFromDb = userDao.findOne("mstrainovic");
        Assert.assertEquals(0, userFromDb.getUserTokens().size());

        //Delete
        userDao.delete("mstrainovic");
        userFromDb = userDao.findOne("mstrainovic");
        Assert.assertNull(userFromDb);
    }

}