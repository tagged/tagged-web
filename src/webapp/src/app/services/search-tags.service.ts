import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable()
export class SearchTagsService {
    searchTerms: any = [];
    constructor(private http: HttpClient) {
    }

    /**
     * Post search tags
     * @param searchTags
     * @returns {Observable<Object>}
     */
    postSearchTags(searchTags) {
        const tags = {
            "tags" : searchTags
        };
        return this.http.post('v1/search', tags);
    }

}
