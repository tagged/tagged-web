import { TestBed, inject } from '@angular/core/testing';

import { SearchTagsService } from './search-tags.service';

describe('SearchTagsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SearchTagsService]
    });
  });

  it('should be created', inject([SearchTagsService], (service: SearchTagsService) => {
    expect(service).toBeTruthy();
  }));
});
