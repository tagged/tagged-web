import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable()
export class TagsService {

  constructor(private http: HttpClient) { }

  /**
   * Post tags data
   * @param tagsData
   */
  postTags(tagsData){
    const tag = {
      fileId: tagsData.fileId,
      name: tagsData.name,
      provider: tagsData.provider,
      providerId: tagsData.providerId,
      tags: tagsData.tags
    };
    this.http.post('v1/tags', tag).subscribe();
  }

  /**
   * Edit tags data
   * @param tagsData
   */
  editTags(tagsData){
    const tag = {
      id: tagsData.id,
      tags: tagsData.tags
    };
    this.http.put('v1/tags', tag).subscribe();
  }

  /**
   * Delete tags data
   * @param tagsData
   */
  deleteTags(tagsData){
    const tag = {
      id: tagsData.id
    };
    const url = 'v1/tags/' + tag.id;
    this.http.delete(url).subscribe();
  }
}
