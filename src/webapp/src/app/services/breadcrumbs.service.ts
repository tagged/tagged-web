import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Injectable()
export class BreadcrumbsService {
    private crumb = new BehaviorSubject<any>('');
    private folderData = new BehaviorSubject<any>('');
    cast = this.crumb.asObservable();
    currentFolderData = this.folderData.asObservable();

    constructor() {
    }

    collectBreadCrumbs(link) {
        this.crumb.next(link);
    }

    loadGrid(data) {
        this.folderData.next(data);
    }
}
