import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class FolderDataService {
    folderData: any;

    constructor(private http: HttpClient) {
    }

    /**
     * Post folderData
     * @param folderData
     * @returns {Observable<Object>}
     */
    postFolderData(folderData) {
        return this.http.post('v1/folder', folderData);
    }
}
