import { TestBed, inject } from '@angular/core/testing';

import { FolderDataService } from './folder-data.service';

describe('FolderDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FolderDataService]
    });
  });

  it('should be created', inject([FolderDataService], (service: FolderDataService) => {
    expect(service).toBeTruthy();
  }));
});
