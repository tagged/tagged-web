import {Component, OnInit, ViewChild} from '@angular/core';
import {NavigationEnd, Router} from "@angular/router";
import {SearchTagsService} from "../services/search-tags.service";
import {MatSort, MatTableDataSource} from "@angular/material";

@Component({
    selector: 'app-search-results',
    templateUrl: './search-results.component.html',
    styleUrls: ['./search-results.component.scss']
})
export class SearchResultsComponent implements OnInit {
    displayedColumns = ['name', 'provider', 'tags'];
    dataSource = new MatTableDataSource();
    @ViewChild(MatSort) sort: MatSort;


  constructor(private router: Router,
              private searchTags: SearchTagsService) {
        this.router.events.subscribe((e: any) => {
            if (e instanceof NavigationEnd) {
                this.searchTags.postSearchTags(searchTags.searchTerms).subscribe((data: any) => {
                  this.dataSource.data = data;
                })
            }
        });
    }

    ngOnInit() {
    }

    ngAfterViewInit() {
      this.dataSource.sort = this.sort;
    }

}
