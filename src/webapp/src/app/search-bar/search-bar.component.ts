import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {SearchTagsService} from "../services/search-tags.service";
import {Router} from "@angular/router";

@Component({
    selector: 'app-search-bar',
    templateUrl: './search-bar.component.html',
    styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit {
    close: boolean = false;
    @Output() closeSearchComponent = new EventEmitter();

    constructor(public searchService: SearchTagsService,
                private router: Router) {
    }

    ngOnInit() {
    }

    /**
     * Fires on enter from search field, sets search terms inside of search-tags service
     * routes view to search results component
     * @param event
     */
    onKeydown(event) {
        event.preventDefault();
        let searchTerms = event.target.value;
        if (searchTerms) {
            this.searchService.searchTerms = searchTerms.split(/[ ,]+/).filter(Boolean);
            this.router.navigate(['/search']);
            this.onClose();
        }
    }

    onClose() {
        this.close = true;
        this.closeSearchComponent.emit(this.close);
    }

}
