import {Component} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {HttpClient} from '@angular/common/http';
import {FolderDataService} from './services/folder-data.service';
import {BreadcrumbsService} from './services/breadcrumbs.service';
import {LoadingMaskService} from 'ngx-loading-mask';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    animations: [
        trigger('flyInOut', [
            state('in', style({transform: 'translateY(0)'})),
            transition('void => *', [
                style({transform: 'translateY(-100%)'}),
                animate(200)
            ]),
            transition('* => void', [
                animate(200, style({transform: 'translateY(-100%)'}))
            ])
        ])
    ]
})
export class AppComponent {
    showList: any;
    breadcrumbs: Array<string> = [];
    searchActivated: any;
    tokens: any[];
    tokensExist: any;
    availableServices: any[];

    constructor(private http: HttpClient,
                public folderService: FolderDataService,
                public breadcrumbsService: BreadcrumbsService,
                public loadingMaskService: LoadingMaskService) {
    }

    ngOnInit() {
        this.http.get('v1/services').subscribe(data => {
            this.collectAvailableServices(data);
        });

        this.http.get('v1/userInfo').subscribe(data => {
            this.collectItems(data);
        });

        this.breadcrumbsService.cast.subscribe(link => {
            this.collectBreadCrumbs(link);
        });
    }

    collectBreadCrumbs(link) {
        if (link) {
            this.breadcrumbs.push(link);
            this.enablePreviousCrumbs();
            this.disableLastBreadCrumb();
            this.disableFirstBreadCrumb();
        }
    }

    enablePreviousCrumbs() {
        this.breadcrumbs.map(crumb => {
            crumb['disabled'] = false;
        });
    }

    disableLastBreadCrumb() {
        const lastLink = this.breadcrumbs[this.breadcrumbs.length - 1];
        lastLink['disabled'] = true;
    }

  /**
   * First element of BreadCrumb is different type so disable it.
   */
  disableFirstBreadCrumb() {
        const firstLink = this.breadcrumbs[0];
        firstLink['disabled'] = true;
    }

    collectAvailableServices(data) {
        this.availableServices = data;
    }

    collectItems(data) {
        this.tokens = data.tokens;
        if (this.tokens.length) {
            this.tokensExist = true;
        }
    }

    openSearchBar(searchActivated) {
        this.searchActivated = !searchActivated;
    }

    closeSearchBar(emitterValue) {
        this.searchActivated = !emitterValue;
    }

    showServices(showList) {
        this.showList = !showList;
    }

    onBreadCrumbClick(breadCrumb) {
        this.loadingMaskService.showGroup();
        breadCrumb.disabled = true;
        this.removeStaleBreadCrumbs(breadCrumb);
        if (this.breadcrumbs.length === 1) {
            breadCrumb.link = '/';
        }
        this.breadcrumbsService.loadGrid(breadCrumb);
    }

    removeStaleBreadCrumbs(breadCrumb) {
        let index = 0;
        let currentBread;
        let length = this.breadcrumbs.length;
        while (length) {
            length--;
            currentBread = this.breadcrumbs[length];
            if (currentBread.name === breadCrumb.name) {
                index = length;
            }
        }
        this.breadcrumbs.splice(index + 1);

    }

    onLinkClick(current) {
      debugger;
        this.loadingMaskService.showGroup();
        if (this.breadcrumbs.length === 0) {
            this.breadcrumbsService.collectBreadCrumbs(current);
        } else {
          this.removeStaleBreadCrumbs(current);

        }
        this.folderService.folderData = {
            path: '/',
            provider: current.tokenIssuer,
            providerId: current.tokenIssuerId
        };
    }

    gotoServiceUrl(url) {
        const currentUrl = window.location.href;
        window.location.href = currentUrl + url;
    }
}

