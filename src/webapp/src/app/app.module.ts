import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {AppComponent} from './app.component';
import {ClientRoutingModule} from './routes';
import 'hammerjs';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
    MatCheckboxModule,
    MatToolbarModule,
    MatSidenavModule,
    MatButtonModule,
    MatListModule,
    MatIconModule,
    MatInputModule,
    MatTableModule,
    MatSortModule,
    MatDialogModule,
    MatChipsModule,
    MatMenuModule,
    MatCardModule
} from '@angular/material';
import {DynamicFolderComponent} from './dynamic-folder/dynamic-folder.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {SearchBarComponent} from './search-bar/search-bar.component';
import {TagDialogComponent} from './tag-dialog/tag-dialog.component';
import {PreferencesComponent} from './preferences/preferences.component';
import {HomeComponent} from './home/home.component';
import {FolderDataService} from './services/folder-data.service';
import {TagsService} from './services/tags.service';
import {SearchTagsService} from './services/search-tags.service';
import {SearchResultsComponent} from './search-results/search-results.component';
import {BreadcrumbsService} from './services/breadcrumbs.service';
import {LoadingMaskModule} from 'ngx-loading-mask';

const maskConfig = {
    snippet: {
        imgUrl: '/assets/img/loading.svg',
        size: 80
    }
};

@NgModule({
    declarations: [
        AppComponent,
        DynamicFolderComponent,
        PageNotFoundComponent,
        SearchBarComponent,
        TagDialogComponent,
        PreferencesComponent,
        HomeComponent,
        SearchResultsComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        ClientRoutingModule,
        MatCheckboxModule,
        MatToolbarModule,
        MatSidenavModule,
        MatButtonModule,
        MatListModule,
        MatIconModule,
        MatInputModule,
        MatTableModule,
        MatSortModule,
        MatDialogModule,
        MatChipsModule,
        MatMenuModule,
        MatCardModule,
        LoadingMaskModule.forRoot(maskConfig)
    ],
    entryComponents: [TagDialogComponent],
    providers: [FolderDataService, TagsService, SearchTagsService, BreadcrumbsService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
