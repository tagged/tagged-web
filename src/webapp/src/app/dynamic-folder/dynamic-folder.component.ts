import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatSort, MatTableDataSource, MatChipsModule} from '@angular/material';
import {TagDialogComponent} from '../tag-dialog/tag-dialog.component';
import {FolderDataService} from '../services/folder-data.service';
import {TagsService} from '../services/tags.service';
import {NavigationEnd, Router} from '@angular/router';
import {BreadcrumbsService} from '../services/breadcrumbs.service';
import {LoadingMaskService} from 'ngx-loading-mask';

@Component({
    selector: 'app-dynamic-folder',
    templateUrl: './dynamic-folder.component.html',
    styleUrls: ['./dynamic-folder.component.scss']
})
export class DynamicFolderComponent implements OnInit {
    displayedColumns = ['name', 'modified', 'tags'];
    dataSource = new MatTableDataSource();
    folderIsEmpty = true;
    @ViewChild(MatSort) sort: MatSort;

    constructor(public dialog: MatDialog,
                public folderService: FolderDataService,
                public tagsService: TagsService,
                public loadingMaskService: LoadingMaskService,
                public breadcrumbsService: BreadcrumbsService,
                private router: Router) {
        this.router.events.subscribe((e: any) => {
            // If it is a NavigationEnd event re-initialise the component
            if (e instanceof NavigationEnd) {
                this.folderService.postFolderData(this.folderService.folderData).subscribe((data: any) => {
                    this.dataSource.data = data;
                    this.folderIsEmpty = this.dataSource.data.length <= 0;
                    this.loadingMaskService.hideGroup();
                });
            }
        });
    }

    ngOnInit() {
        this.breadcrumbsService.currentFolderData.subscribe(data => {
            this.reloadFolder(data);
        });
    }

    /**
     * Set the sort after the view init since this component will
     * be able to query its view for the initialized sort.
     */
    ngAfterViewInit() {
        this.dataSource.sort = this.sort;
    }

    reloadFolder(item) {
        if (item) {
            this.folderService.postFolderData({
                path: item.path,
                provider: item.provider,
                providerId: item.providerId
            }).subscribe((data: any) => {
                this.dataSource.data = data;
                this.folderIsEmpty = this.dataSource.data.length <= 0;
                this.loadingMaskService.hideGroup();
            });
        }
    }

    onLinkClick(item) {
        this.loadingMaskService.showGroup();
        this.breadcrumbsService.collectBreadCrumbs(item);
        this.folderService.postFolderData({
            path: item.path,
            provider: item.provider,
            providerId: item.providerId
        }).subscribe((data: any) => {
            this.dataSource.data = data;
            this.folderIsEmpty = this.dataSource.data.length <= 0;
            this.loadingMaskService.hideGroup();
        });
    }

    /**
     * Compares tag state before and after editing, calls post/edit/delete service based on comparison results
     * @param result
     */
    manipulateTagsData(result) {
        const currentTags = result.tags;
        const previousTags = result.priorTags;

        if (previousTags.length) {
            if (currentTags.length && previousTags.length) {
                this.tagsService.editTags(result);
            }
            else {
                this.tagsService.deleteTags(result);
            }
        }
        else {
            if (currentTags && currentTags.length) {
                this.tagsService.postTags(result);
            }
        }
    }


    openTagDialog(currentItem) {

        // copy current tags in separate array in order to compare them later
        currentItem.priorTags = Object.assign([], currentItem.tags);

        // create dialog with tags
        const dialogRef = this.dialog.open(TagDialogComponent, {
            width: '600px',
            data: currentItem
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.manipulateTagsData(result);
            }
        });
    }
}
