import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Component({
    selector: 'app-preferences',
    templateUrl: './preferences.component.html',
    styleUrls: ['./preferences.component.scss']
})
export class PreferencesComponent implements OnInit {

    constructor(private http: HttpClient) {
    }

    ngOnInit() {
    }

    /**
     * Logout handler
     */
    onLogout() {
        this.http.get('logout').subscribe();
        // Reload app after logout
        location.reload();
    }
}
