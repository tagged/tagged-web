package net.strainovic.tagged.taggedweb.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("dropBoxAuth")
public class DropBoxAuthConfiguration {

    private String key;

    private String secret;

    public String getKey() {
        return key;
    }

    public String getSecret() {
        return secret;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }
}
