package net.strainovic.tagged.taggedweb.dao;

import net.strainovic.tagged.taggedweb.model.User;
import org.springframework.data.repository.CrudRepository;

public interface UserDao extends CrudRepository<User, String> {

}