package net.strainovic.tagged.taggedweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TaggedWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(TaggedWebApplication.class, args);

    }
}
