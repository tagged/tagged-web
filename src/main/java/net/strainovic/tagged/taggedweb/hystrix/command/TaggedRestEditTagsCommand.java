package net.strainovic.tagged.taggedweb.hystrix.command;

import java.util.Collection;

public class TaggedRestEditTagsCommand {

    private String id;

    private Collection<String> tags;

    public String getId() {
        return id;
    }

    public Collection<String> getTags() {
        return tags;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setTags(Collection<String> tags) {
        this.tags = tags;
    }
}
