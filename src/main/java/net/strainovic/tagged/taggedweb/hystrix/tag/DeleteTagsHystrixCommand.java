package net.strainovic.tagged.taggedweb.hystrix.tag;

import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.netflix.hystrix.HystrixThreadPoolKey;
import net.strainovic.tagged.taggedweb.hystrix.HystrixUrls;
import net.strainovic.tagged.taggedweb.hystrix.command.TaggedRestAddTagsCommand;
import net.strainovic.tagged.taggedweb.util.LogHelper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class DeleteTagsHystrixCommand extends HystrixCommand<Void> {

    private static final Log LOG = LogFactory.getLog(DeleteTagsHystrixCommand.class);

    private RestTemplate restTemplate;

    private String id;


    public DeleteTagsHystrixCommand(RestTemplate restTemplate, String id) {
        super(HystrixCommandGroupKey.Factory.asKey("Tags"), HystrixThreadPoolKey.Factory.asKey("Delete"));
        this.id = id;
        this.restTemplate = restTemplate;
    }

    @Override
    protected Void run() {
        LogHelper.debug(LOG, "Deleting tags for document id: %s",
                this.id);

        String url = HystrixUrls.TAGGED_REST_V1_TAGS + "/" + this.id;

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        HttpEntity<TaggedRestAddTagsCommand> request = new HttpEntity<>(headers);
        ResponseEntity<Void> response = restTemplate.exchange(url, HttpMethod.DELETE, request,
                        new ParameterizedTypeReference<Void>() {
                        });

        LogHelper.debug(LOG, "Deleting tags tagged-rest return %s response code", response
                .getStatusCode());

        if (!response.getStatusCode().is2xxSuccessful()) {
            LogHelper.debug(LOG, "Error trying to delete tags from tagged-rest");
            return null;
        }

        return null;
    }

    @Override
    protected Void getFallback() {
        LogHelper.warn(LOG, "Execution Fallback");
        //mstrainovic - For further improvement deal with Fallback (try again or something)
        return null;
    }
}
