package net.strainovic.tagged.taggedweb.hystrix.command;

import java.util.Collection;

public class TaggedRestAddTagsCommand {

    private String fileId;

    private String name;

    private String provider;

    private String providerId;

    private Collection<String> tags;

    private String username;

    public String getFileId() {
        return fileId;
    }

    public String getName() {
        return name;
    }

    public String getProvider() {
        return provider;
    }

    public String getProviderId() {
        return providerId;
    }

    public Collection<String> getTags() {
        return tags;
    }

    public String getUsername() {
        return username;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }


    public void setName(String name) {
        this.name = name;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public void setTags(Collection<String> tags) {
        this.tags = tags;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
