package net.strainovic.tagged.taggedweb.hystrix;

import com.netflix.hystrix.HystrixExecutable;
import net.strainovic.tagged.taggedweb.controller.dto.FileDto;
import net.strainovic.tagged.taggedweb.hystrix.dropbox.FolderDropboxHystrixCommand;
import net.strainovic.tagged.taggedweb.model.TokenIssuer;
import net.strainovic.tagged.taggedweb.model.User;
import net.strainovic.tagged.taggedweb.model.UserToken;
import net.strainovic.tagged.taggedweb.util.LogHelper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Optional;

public class HystrixCommandFactory {

    private static final Log LOG = LogFactory.getLog(HystrixCommandFactory.class);

    public static HystrixExecutable<List<FileDto>> getFolderCommand(User user, TokenIssuer provider, String providerId,
                                                                    RestTemplate restTemplate, String path) {
        HystrixExecutable<List<FileDto>> command;

        Optional<UserToken> optionalAuthHeader = user.getUserTokens().stream()
                .filter(ut -> provider.equals(ut.getTokenIssuer()) && providerId.equals(ut.getTokenIssuerId()))
                .findFirst();
        if (!optionalAuthHeader.isPresent()) {
            String errorMessage = String
                    .format("Missing user token for user %s and provider %s", user.getUserId(), provider);

            LogHelper.error(LOG, errorMessage);
            throw new RuntimeException(errorMessage);
        }

        String authToken = optionalAuthHeader.get().getTokenValue();

        switch (provider) {
            case DROPBOX:
                command = new FolderDropboxHystrixCommand(authToken, restTemplate, path);
                break;
            default:
                LogHelper.error(LOG, "Could not instantiate Hystrix Command with provider %s", provider);
                throw new RuntimeException(String
                        .format("Could not instantiate Hystrix Command with provider %s", provider));
        }

        return command;
    }

}
