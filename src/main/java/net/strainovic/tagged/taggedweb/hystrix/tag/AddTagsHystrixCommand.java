package net.strainovic.tagged.taggedweb.hystrix.tag;

import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.netflix.hystrix.HystrixThreadPoolKey;
import net.strainovic.tagged.taggedweb.controller.dto.TagDto;
import net.strainovic.tagged.taggedweb.hystrix.HystrixUrls;
import net.strainovic.tagged.taggedweb.hystrix.command.TaggedRestAddTagsCommand;
import net.strainovic.tagged.taggedweb.util.LogHelper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class AddTagsHystrixCommand extends HystrixCommand<TagDto> {

    private static final Log LOG = LogFactory.getLog(AddTagsHystrixCommand.class);

    private RestTemplate restTemplate;

    private TaggedRestAddTagsCommand command;


    public AddTagsHystrixCommand(RestTemplate restTemplate, TaggedRestAddTagsCommand command) {
        super(HystrixCommandGroupKey.Factory.asKey("Tags"), HystrixThreadPoolKey.Factory.asKey("Add"));
        this.command = command;
        this.restTemplate = restTemplate;
    }

    @Override
    protected TagDto run() {
        LogHelper.debug(LOG, "Adding tags: %s for document id: %s and provider %s for user: %s",
                command.getTags(), command.getFileId(), command.getProvider(), command.getUsername());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        HttpEntity<TaggedRestAddTagsCommand> request = new HttpEntity<>(command, headers);
        ResponseEntity<TagDto> response = restTemplate
                .exchange(HystrixUrls.TAGGED_REST_V1_TAGS, HttpMethod.POST, request,
                        new ParameterizedTypeReference<TagDto>() {
                        });

        LogHelper.debug(LOG, "Getting tags tagged-rest return %s response code", response
                .getStatusCode());

        if (!response.getStatusCode().is2xxSuccessful()) {
            return null;
        }

        return response.getBody();
    }

    @Override
    protected TagDto getFallback() {
        LogHelper.warn(LOG, "Execution Fallback");
        //mstrainovic - For further improvement deal with Fallback (try again or something)
        return null;
    }
}
