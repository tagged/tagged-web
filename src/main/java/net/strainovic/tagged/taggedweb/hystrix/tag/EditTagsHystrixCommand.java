package net.strainovic.tagged.taggedweb.hystrix.tag;

import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.netflix.hystrix.HystrixThreadPoolKey;
import net.strainovic.tagged.taggedweb.controller.dto.TagDto;
import net.strainovic.tagged.taggedweb.hystrix.HystrixUrls;
import net.strainovic.tagged.taggedweb.hystrix.command.TaggedRestEditTagsCommand;
import net.strainovic.tagged.taggedweb.util.LogHelper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class EditTagsHystrixCommand extends HystrixCommand<TagDto> {

    private static final Log LOG = LogFactory.getLog(EditTagsHystrixCommand.class);

    private RestTemplate restTemplate;

    private TaggedRestEditTagsCommand command;


    public EditTagsHystrixCommand(RestTemplate restTemplate, TaggedRestEditTagsCommand command) {
        super(HystrixCommandGroupKey.Factory.asKey("Tags"), HystrixThreadPoolKey.Factory.asKey("Edit"));
        this.command = command;
        this.restTemplate = restTemplate;
    }

    @Override
    protected TagDto run() {
        LogHelper.debug(LOG, "Edit tags: %s for document id: %s",
                command.getTags(), command.getId());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        HttpEntity<TaggedRestEditTagsCommand> request = new HttpEntity<>(command, headers);
        ResponseEntity<TagDto> response = restTemplate
                .exchange(HystrixUrls.TAGGED_REST_V1_TAGS, HttpMethod.PUT, request,
                        new ParameterizedTypeReference<TagDto>() {
                        });

        LogHelper.debug(LOG, "Edit tags tagged-rest return %s response code", response
                .getStatusCode());

        if (!response.getStatusCode().is2xxSuccessful()) {
            return null;
        }

        return response.getBody();
    }

    @Override
    protected TagDto getFallback() {
        LogHelper.warn(LOG, "Execution Fallback");
        //mstrainovic - For further improvement deal with Fallback (try again or something)
        return null;
    }
}
