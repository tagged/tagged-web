package net.strainovic.tagged.taggedweb.hystrix.command;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Collection;
import java.util.List;

@ApiModel
public class TaggedRestSearchCommand {

    @ApiModelProperty
    private List<String> fileIds;

    @ApiModelProperty
    private String provider;

    @ApiModelProperty
    private Collection<String> tags;

    @ApiModelProperty
    private String username;

    public List<String> getFileIds() {
        return fileIds;
    }

    public String getProvider() {
        return provider;
    }

    public Collection<String> getTags() {
        return tags;
    }

    public String getUsername() {
        return username;
    }

    public void setFileIds(List<String> fileIds) {
        this.fileIds = fileIds;
    }

    public void setProvider(String provider) {
        this.provider = provider.toUpperCase();
    }

    public void setTags(Collection<String> tags) {
        this.tags = tags;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
