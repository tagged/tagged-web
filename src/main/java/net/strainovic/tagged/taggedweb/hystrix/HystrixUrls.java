package net.strainovic.tagged.taggedweb.hystrix;

public final class HystrixUrls {

    public static final String TAGGED_BOX_V1_FOLDER_URL = "http://tagged-box/v1/folder";

    public static final String TAGGED_REST_V1_SEARCH = "http://tagged-rest/v1/tags/search";

    public static final String TAGGED_REST_V1_TAGS = "http://tagged-rest/v1/tags";

}
