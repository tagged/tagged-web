package net.strainovic.tagged.taggedweb.hystrix.dropbox;

import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.netflix.hystrix.HystrixThreadPoolKey;
import net.strainovic.tagged.taggedweb.controller.dto.FileDto;
import net.strainovic.tagged.taggedweb.hystrix.HystrixUrls;
import net.strainovic.tagged.taggedweb.util.LogHelper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FolderDropboxHystrixCommand extends HystrixCommand<List<FileDto>> {

    private static final Log LOG = LogFactory.getLog(FolderDropboxHystrixCommand.class);

    private String path;

    private RestTemplate restTemplate;

    private String authToken;

    private boolean retry;

    public FolderDropboxHystrixCommand(String authToken, RestTemplate restTemplate, String path) {
        super(HystrixCommandGroupKey.Factory.asKey("Dropbox"), HystrixThreadPoolKey.Factory.asKey("Folder"), 7000);
        this.authToken = authToken;
        this.restTemplate = restTemplate;
        this.path = path;
        this.retry = true;
    }

    @Override
    protected List<FileDto> run() {
        LogHelper.debug(LOG, "Getting folder list from tagged-box for path: %s", path);

        Map<String, String> requestBody = new HashMap<>();
        requestBody.put("path", path);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        headers.add(HttpHeaders.AUTHORIZATION, "Bearer " + authToken);
        HttpEntity<Map<String, String>> request = new HttpEntity<>(requestBody, headers);
        ResponseEntity<List<FileDto>> response = restTemplate
                .exchange(HystrixUrls.TAGGED_BOX_V1_FOLDER_URL, HttpMethod.POST, request,
                        new ParameterizedTypeReference<List<FileDto>>() {
                        });

        LogHelper.debug(LOG, "Getting folder list from tagged-box return %s response code", response
                .getStatusCode());

        if (!response.getStatusCode().is2xxSuccessful()) {
            return new ArrayList<>();
        }

        List<FileDto> files = response.getBody();
        LogHelper.debug(LOG, "Folder search from tagged-box return %s file", files.size());

        return files;
    }

    @Override
    protected List<FileDto> getFallback() {
        LogHelper.warn(LOG, "Execution Fallback");
        List<FileDto> fallbackResult = new ArrayList<>();

        if (retry) {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

            //Only one retry
            LogHelper.debug(LOG, "RETRY - Getting folder list from tagged-box for path: %s", path);
            FolderDropboxHystrixCommand retryCommand = new FolderDropboxHystrixCommand(this.authToken, this.restTemplate, this.path);
            retryCommand.retry = false;
            fallbackResult = retryCommand.execute();
        }

        return fallbackResult;
    }
}
