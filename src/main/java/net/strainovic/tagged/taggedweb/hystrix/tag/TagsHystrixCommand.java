package net.strainovic.tagged.taggedweb.hystrix.tag;

import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.netflix.hystrix.HystrixThreadPoolKey;
import net.strainovic.tagged.taggedweb.controller.dto.TagDto;
import net.strainovic.tagged.taggedweb.hystrix.HystrixUrls;
import net.strainovic.tagged.taggedweb.hystrix.command.TaggedRestSearchCommand;
import net.strainovic.tagged.taggedweb.util.LogHelper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

public class TagsHystrixCommand extends HystrixCommand<List<TagDto>> {

    private static final Log LOG = LogFactory.getLog(TagsHystrixCommand.class);

    private RestTemplate restTemplate;

    private TaggedRestSearchCommand command;

    private boolean retry;


    public TagsHystrixCommand(RestTemplate restTemplate, TaggedRestSearchCommand command, boolean retry) {
        super(HystrixCommandGroupKey.Factory.asKey("Tags"), HystrixThreadPoolKey.Factory.asKey("Search"));
        this.command = command;
        this.restTemplate = restTemplate;
        this.retry = retry;
    }

    @Override
    protected List<TagDto> run() {
        LogHelper.debug(LOG, "Getting tags from tagged-rest for user: %s, provider: %s, ids: %s",
                command.getUsername(), command.getProvider(), command.getFileIds());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        HttpEntity<TaggedRestSearchCommand> request = new HttpEntity<>(command, headers);
        ResponseEntity<List<TagDto>> response = restTemplate
                .exchange(HystrixUrls.TAGGED_REST_V1_SEARCH, HttpMethod.POST, request,
                        new ParameterizedTypeReference<List<TagDto>>() {
                        });

        LogHelper.debug(LOG, "Getting tags tagged-rest return %s response code", response
                .getStatusCode());

        if (!response.getStatusCode().is2xxSuccessful()) {
            return new ArrayList<>();
        }

        List<TagDto> tags = response.getBody();
        LogHelper.debug(LOG, "Getting tags from tagged-rest return %s tags", tags.size());

        return tags;
    }

    @Override
    protected List<TagDto> getFallback() {
        LogHelper.warn(LOG, "Execution Fallback");
        List<TagDto> fallbackResult = new ArrayList<>();

        if (retry) {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

            //Only one retry
            LogHelper.warn(LOG, "RETRY - Getting tags from tagged-rest for user: %s, provider: %s, ids: %s",
                    command.getUsername(), command.getProvider(), command.getFileIds());
            TagsHystrixCommand tagsHystrixCommand = new TagsHystrixCommand(this.restTemplate, this.command, false);
            fallbackResult = tagsHystrixCommand.execute();
        }

        return fallbackResult;
    }
}
