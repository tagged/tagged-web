package net.strainovic.tagged.taggedweb.model.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = TagsValidator.class)
@Documented
public @interface Tags {

    String message() default "For tag only leather or digit are allowed";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };
}
