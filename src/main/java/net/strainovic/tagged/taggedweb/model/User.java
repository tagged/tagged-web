package net.strainovic.tagged.taggedweb.model;

import org.hibernate.annotations.BatchSize;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Entity
@BatchSize(size = 100)
@Table(name = "user")
public class User {

    public User() {
    }

    public User(String userId) {
        this.userId = userId;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "userId")
    public Set<UserToken> userTokens;

    @Id
    public String userId;

    public void addUserToken(UserToken userToken) {
        if (userTokens == null) {
            userTokens = new HashSet<>();
        }

        if (userTokens.contains(userToken)) {
            userTokens.remove(userToken);
        }

        userTokens.add(userToken);
    }

    public Optional<UserToken> findTokenByIssuer(TokenIssuer tokenIssuer) {
        if (userTokens != null) {
            return userTokens.stream().filter(t -> t.getTokenIssuer().equals(tokenIssuer)).findFirst();
        }

        return Optional.empty();
    }

    public Set<UserToken> getUserTokens() {
        return userTokens;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserTokens(Set<UserToken> userTokens) {
        this.userTokens = userTokens;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
