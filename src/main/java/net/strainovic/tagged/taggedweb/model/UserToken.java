package net.strainovic.tagged.taggedweb.model;

import org.hibernate.annotations.BatchSize;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@BatchSize(size = 100)
@Table(name = "user_token")
public class UserToken {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private TokenIssuer tokenIssuer;

    @Column
    private String tokenIssuerId;

    @Column(nullable = false)
    private String tokenValue;

    public UserToken() {
    }

    public UserToken(TokenIssuer tokenIssuer, String tokenValue, String tokenIssuerId) {
        this.tokenIssuer = tokenIssuer;
        this.tokenValue = tokenValue;
        this.tokenIssuerId = tokenIssuerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserToken userToken = (UserToken) o;
        return getTokenIssuer() == userToken.getTokenIssuer() &&
                Objects.equals(getTokenIssuerId(), userToken.getTokenIssuerId()) &&
                Objects.equals(getTokenValue(), userToken.getTokenValue());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTokenIssuer(), getTokenIssuerId(), getTokenValue());
    }

    public Long getId() {
        return id;
    }

    public TokenIssuer getTokenIssuer() {
        return tokenIssuer;
    }

    public String getTokenIssuerId() {
        return tokenIssuerId;
    }

    public String getTokenValue() {
        return tokenValue;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setTokenIssuer(TokenIssuer tokenIssuer) {
        this.tokenIssuer = tokenIssuer;
    }

    public void setTokenIssuerId(String tokenIssuerId) {
        this.tokenIssuerId = tokenIssuerId;
    }

    public void setTokenValue(String tokenValue) {
        this.tokenValue = tokenValue;
    }
}
