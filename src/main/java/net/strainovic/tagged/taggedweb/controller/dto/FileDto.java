package net.strainovic.tagged.taggedweb.controller.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.strainovic.tagged.taggedweb.model.TokenIssuer;
import org.hibernate.validator.constraints.NotBlank;

import java.util.Collection;
import java.util.Date;

@ApiModel
public class FileDto extends TagDto {

    @ApiModelProperty
    private boolean folder;

    @ApiModelProperty
    private Date modified;

    @ApiModelProperty
    private String name;

    @ApiModelProperty
    private String path;

    public FileDto(String id, String fileId, TokenIssuer provider, String providerId, boolean folder, Date modified, String name, String path, Collection<String> tags) {
        super(id, fileId, provider, providerId, tags);
        this.folder = folder;
        this.modified = modified;
        this.name = name;
        this.path = path;
    }

    public FileDto() {
    }

    public Date getModified() {
        return modified;
    }

    public String getName() {
        return name;
    }

    public String getPath() {
        return path;
    }

    public boolean isFolder() {
        return folder;
    }

    public void setFolder(boolean folder) {
        this.folder = folder;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
