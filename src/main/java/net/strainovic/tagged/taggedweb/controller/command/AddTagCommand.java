package net.strainovic.tagged.taggedweb.controller.command;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.strainovic.tagged.taggedweb.model.TokenIssuer;
import net.strainovic.tagged.taggedweb.model.validation.Tags;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.util.Collection;

@ApiModel
public class AddTagCommand {

    @NotBlank
    @ApiModelProperty(required = true, example = "someRandomId")
    private String fileId;

    @NotBlank
    @ApiModelProperty(required = true, example = "Some file name.docx")
    private String name;

    @NotNull
    @ApiModelProperty(example = "DROPBOX", notes = "Folder search provider")
    private TokenIssuer provider;

    @NotNull
    @ApiModelProperty(example = "someTokenIssuerId", notes = "Token issuer id receved from userInfo")
    private String providerId;

    @Tags
    @ApiModelProperty(required = true)
    private Collection<String> tags;

    public String getFileId() {
        return fileId;
    }

    public String getName() {
        return name;
    }

    public TokenIssuer getProvider() {
        return provider;
    }

    public String getProviderId() {
        return providerId;
    }

    public Collection<String> getTags() {
        return tags;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setProvider(TokenIssuer provider) {
        this.provider = provider;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public void setTags(Collection<String> tags) {
        this.tags = tags;
    }
}
