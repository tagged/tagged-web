package net.strainovic.tagged.taggedweb.controller.command;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.strainovic.tagged.taggedweb.model.validation.Tags;
import org.hibernate.validator.constraints.NotBlank;

import java.util.Collection;

@ApiModel
public class EditTagCommand {

    @NotBlank
    @ApiModelProperty(required = true, example = "someRandomId")
    private String id;

    @Tags
    @ApiModelProperty(required = true)
    private Collection<String> tags;

    public String getId() {
        return id;
    }

    public Collection<String> getTags() {
        return tags;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setTags(Collection<String> tags) {
        this.tags = tags;
    }
}
