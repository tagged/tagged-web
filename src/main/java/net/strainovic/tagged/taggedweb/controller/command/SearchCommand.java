package net.strainovic.tagged.taggedweb.controller.command;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.strainovic.tagged.taggedweb.model.validation.Tags;

import java.util.Collection;

@ApiModel
public class SearchCommand {

    @Tags
    @ApiModelProperty(required = true)
    private Collection<String> tags;

    public Collection<String> getTags() {
        return tags;
    }

    public void setTags(Collection<String> tags) {
        this.tags = tags;
    }
}
