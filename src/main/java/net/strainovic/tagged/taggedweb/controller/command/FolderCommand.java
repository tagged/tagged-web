package net.strainovic.tagged.taggedweb.controller.command;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.strainovic.tagged.taggedweb.model.TokenIssuer;
import org.hibernate.validator.constraints.NotBlank;

@ApiModel
public class FolderCommand {

    @NotBlank
    @ApiModelProperty(required = true, example = "/", notes = "Specify the root folder as an empty string rather than as \"/\"")
    private String path;

    @ApiModelProperty(required = true, example = "DROPBOX", notes = "Folder search provider")
    private TokenIssuer provider;

    @ApiModelProperty(required = true, example = "someProviderId", notes = "Folder search provider id")
    private String providerId;

    public String getPath() {
        return path;
    }

    public TokenIssuer getProvider() {
        return provider;
    }

    public String getProviderId() {
        return providerId;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setProvider(TokenIssuer provider) {
        this.provider = provider;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }
}
