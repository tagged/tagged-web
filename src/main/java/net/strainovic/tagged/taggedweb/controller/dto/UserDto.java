package net.strainovic.tagged.taggedweb.controller.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

@ApiModel
public class UserDto {

    @ApiModelProperty
    private List<TokenDto> tokens;

    @ApiModelProperty
    private String userId;

    public UserDto() {
    }

    public UserDto(String userId) {
        this.userId = userId;
    }

    public List<TokenDto> getTokens() {
        return tokens;
    }

    public String getUserId() {
        return userId;
    }

    public void setTokens(List<TokenDto> tokens) {
        this.tokens = tokens;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
