package net.strainovic.tagged.taggedweb.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import net.strainovic.tagged.taggedweb.controller.command.SearchCommand;
import net.strainovic.tagged.taggedweb.controller.dto.TagDto;
import net.strainovic.tagged.taggedweb.hystrix.command.TaggedRestSearchCommand;
import net.strainovic.tagged.taggedweb.service.TagService;
import net.strainovic.tagged.taggedweb.util.LogHelper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.security.Principal;
import java.util.List;

@Api(value = "Search", tags = "Search")
@RestController
public class SearchController {

    private static final Log LOG = LogFactory.getLog(SearchController.class);

    @Autowired
    private TagService tagService;

    @ApiOperation(value = "Search", httpMethod = "POST")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Return list of files from all providers for specific tags"),
            @ApiResponse(code = 400, message = "Invalid request parameters")})
    @PostMapping("/v1/search")
    public List<TagDto> search(@RequestBody @NotNull @Valid SearchCommand searchCommand, final Principal principal) {
        String username = principal.getName();

        LogHelper.debug(LOG, "User: %s searching tags: %s", username, searchCommand.getTags());

        TaggedRestSearchCommand command = new TaggedRestSearchCommand();
        command.setUsername(username);
        command.setTags(searchCommand.getTags());

        return getTagService().search(command);
    }

    private TagService getTagService() {
        return tagService;
    }

}
