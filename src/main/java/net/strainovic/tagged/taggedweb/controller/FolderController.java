package net.strainovic.tagged.taggedweb.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import net.strainovic.tagged.taggedweb.controller.command.FolderCommand;
import net.strainovic.tagged.taggedweb.controller.dto.FileDto;
import net.strainovic.tagged.taggedweb.controller.dto.TagDto;
import net.strainovic.tagged.taggedweb.hystrix.command.TaggedRestSearchCommand;
import net.strainovic.tagged.taggedweb.service.FolderService;
import net.strainovic.tagged.taggedweb.service.TagService;
import net.strainovic.tagged.taggedweb.util.LogHelper;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.security.Principal;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Api(value = "Folder", tags = "Folder")
@RestController
public class FolderController {

    private static final Log LOG = LogFactory.getLog(FolderController.class);

    @Autowired
    private FolderService folderService;

    @Autowired
    private TagService tagService;

    @ApiOperation(value = "Folder search", httpMethod = "POST")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Return list of files from requested folder"),
            @ApiResponse(code = 400, message = "Invalid request parameters")})
    @PostMapping("/v1/folder")
    public List<FileDto> folder(@RequestBody @NotNull @Valid FolderCommand folderCommand, final Principal principal) {
        LogHelper.debug(LOG, "Getting folder data for path: %s and provider: %s",
                folderCommand.getPath(), folderCommand.getProvider());

        String username = principal.getName();

        //First do search in provider like DROPBOX, GOOGLE_DRIVE ...
        List<FileDto> files = getFolderService()
                .folderSearch(username, folderCommand.getProvider(),
                        folderCommand.getProviderId(), folderCommand.getPath());
        LogHelper.debug(LOG, "Returned %s files from path: %s and provider: %s", files.size(),
                folderCommand.getPath(), folderCommand.getProvider());

        if (CollectionUtils.isNotEmpty(files)) {
            List<String> fileIds = files.stream().map(FileDto::getFileId)
                    .filter(Objects::nonNull).collect(Collectors.toList());

            TaggedRestSearchCommand command = new TaggedRestSearchCommand();
            command.setProvider(folderCommand.getProvider().name());
            command.setUsername(username);
            command.setFileIds(fileIds);

            //Than find tags for data from provider
            List<TagDto> tags = getTagService().search(command);

            if (CollectionUtils.isNotEmpty(tags)) {
                Map<String, TagDto> tagMap = tags.stream().collect(Collectors.toMap(TagDto::getFileId, t -> t));

                files.forEach(f -> {
                    TagDto tag = tagMap.get(f.getFileId());
                    if (tag != null) {
                        f.setId(tag.getId());
                        f.setTags(tag.getTags());
                    }
                });
            }
        }

        return files;
    }

    private FolderService getFolderService() {
        return folderService;
    }

    private TagService getTagService() {
        return tagService;
    }

}
