package net.strainovic.tagged.taggedweb.controller.dropbox;

import com.dropbox.core.DbxAppInfo;
import com.dropbox.core.DbxAuthFinish;
import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.DbxSessionStore;
import com.dropbox.core.DbxStandardSessionStore;
import com.dropbox.core.DbxWebAuth;
import com.dropbox.core.util.LangUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import net.strainovic.tagged.taggedweb.config.DropBoxAuthConfiguration;
import net.strainovic.tagged.taggedweb.dao.UserDao;
import net.strainovic.tagged.taggedweb.model.TokenIssuer;
import net.strainovic.tagged.taggedweb.model.User;
import net.strainovic.tagged.taggedweb.model.UserToken;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.Principal;
import java.util.Set;
import java.util.stream.Collectors;

@Api(value = "Authorization", description = "Dropbox authorization", consumes = "application/json", tags = "Auth")
@Controller
public class DropBoxAuth {

    private static final Log LOGGER = LogFactory.getLog(DropBoxAuth.class);

    @Autowired
    private DropBoxAuthConfiguration dropBoxAuthConfiguration;

    @Autowired
    private UserDao userDao;

    @ApiOperation(value = "Dropbox on finish auth return to this url", httpMethod = "GET")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Redirect to root /"),
            @ApiResponse(code = 400, message = "Invalid request parameters")})
    @GetMapping(value = "/v1/dropboxAuthFinish")
    public void doFinish(HttpServletRequest request, HttpServletResponse response, final Principal principal)
            throws IOException {
        DbxAuthFinish authFinish;
        try {
            authFinish = getWebAuth(request).finishFromRedirect(
                    getRedirectUri(request),
                    getSessionStore(request),
                    request.getParameterMap()
            );
        } catch (DbxWebAuth.BadRequestException e) {
            LOGGER.warn("On /dropbox-auth-finish: Bad request: " + e.getMessage());
            response.sendError(400);
            return;
        } catch (DbxWebAuth.BadStateException e) {
            // Send them back to the start of the auth flow.
            response.sendRedirect(getUrl(request, "/v1/dropboxAuth"));
            return;
        } catch (DbxWebAuth.CsrfException e) {
            LOGGER.warn("On /dropbox-auth-finish: CSRF mismatch: " + e.getMessage());
            response.sendError(403);
            return;
        } catch (DbxWebAuth.NotApprovedException e) {
            LOGGER.warn("Not approved?");
            return;
        } catch (DbxWebAuth.ProviderException e) {
            LOGGER.warn("On /dropbox-auth-finish: Auth failed: " + e.getMessage());
            response.sendError(503, "Error communicating with Dropbox.");
            return;
        } catch (DbxException e) {
            LOGGER.warn("On /dropbox-auth-finish: Error getting token: " + e);
            response.sendError(503, "Error communicating with Dropbox.");
            return;
        }

        // We have an Dropbox API access token now.  This is what will let us make Dropbox API
        // calls.  Save it in the database entry for the current user.
        String accessToken = authFinish.getAccessToken();
        String dropboxUserId = authFinish.getUserId();
        String username = principal.getName();

        saveDropboxUserToken(username, accessToken, dropboxUserId);

        response.sendRedirect("/");
    }

    @ApiOperation(value = "Authorize Dropbox app", httpMethod = "GET")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Redirect to root authorize url"),
            @ApiResponse(code = 400, message = "Invalid request parameters")})
    @GetMapping(value = "/v1/dropboxAuth")
    public void doStart(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        // Start the authorization process with Dropbox.
        DbxWebAuth.Request authRequest = DbxWebAuth.newRequestBuilder()
                // After we redirect the user to the Dropbox website for authorization,
                // Dropbox will redirect them back here.
                .withRedirectUri(getRedirectUri(request), getSessionStore(request))
                .build();
        String authorizeUrl = getWebAuth(request).authorize(authRequest);

        // Redirect the user to the Dropbox website so they can approve our application.
        // The Dropbox website will send them back to /dropbox-auth-finish when they're done.
        response.sendRedirect(authorizeUrl);
    }

    @ApiOperation(value = "Remove Dropbox authorization", httpMethod = "POST")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Removing dropbox auth and redirect to /"),
            @ApiResponse(code = 400, message = "Invalid request parameters")})
    @PostMapping(value = "/v1/dropboxAuthUnlink")
    public void doUnlink(HttpServletRequest request, HttpServletResponse response, final Principal principal)
            throws IOException {
        removeDropboxUserToken(principal.getName());

        response.sendRedirect("/");
    }

    private DropBoxAuthConfiguration getDropBoxAuthConfiguration() {
        return dropBoxAuthConfiguration;
    }

    private String getRedirectUri(final HttpServletRequest request) {
        return getUrl(request, "/v1/dropboxAuthFinish");
    }

    private DbxSessionStore getSessionStore(final HttpServletRequest request) {
        // Select a spot in the session for DbxWebAuth to store the CSRF token.
        HttpSession session = request.getSession(true);
        String sessionKey = "dropbox-auth-csrf-token";
        return new DbxStandardSessionStore(session, sessionKey);
    }

    private String getUrl(HttpServletRequest request, String path) {
        URL requestUrl;
        try {
            requestUrl = new URL(request.getRequestURL().toString());
            return new URL(requestUrl, path).toExternalForm();
        } catch (MalformedURLException ex) {
            throw LangUtil.mkAssert("Bad URL", ex);
        }
    }

    private UserDao getUserDao() {
        return userDao;
    }

    private DbxWebAuth getWebAuth(final HttpServletRequest request) {
        String key = getDropBoxAuthConfiguration().getKey();
        String secret = getDropBoxAuthConfiguration().getSecret();

        DbxAppInfo dbxAppInfo = new DbxAppInfo(key, secret);

        DbxRequestConfig dbxRequestConfig = DbxRequestConfig.newBuilder("tagged-box")
                .withUserLocaleFrom(request.getLocale())
                .build();

        return new DbxWebAuth(dbxRequestConfig, dbxAppInfo);
    }

    private void removeDropboxUserToken(String username) {
        User user = getUserDao().findOne(username);

        if (user != null) {
            Set<UserToken> filteredTokens = user.getUserTokens().stream()
                    .filter(ut -> ut.getTokenIssuer().equals(TokenIssuer.DROPBOX)).collect(Collectors.toSet());
            user.setUserTokens(filteredTokens);
            userDao.save(user);
        }
    }

    private void saveDropboxUserToken(String username, String accessToken, String dropBoxUserId) {
        User user = getUserDao().findOne(username);

        if (user == null) {
            user = new User(username);
        }

        user.addUserToken(new UserToken(TokenIssuer.DROPBOX, accessToken, dropBoxUserId));
        userDao.save(user);
    }
}
