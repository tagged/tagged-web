package net.strainovic.tagged.taggedweb.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import net.strainovic.tagged.taggedweb.controller.dto.TokenDto;
import net.strainovic.tagged.taggedweb.controller.dto.UserDto;
import net.strainovic.tagged.taggedweb.dao.UserDao;
import net.strainovic.tagged.taggedweb.model.User;
import net.strainovic.tagged.taggedweb.model.UserToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Api(value = "User", description = "Getting user details", consumes = "application/json", tags = "User")
@RestController
public class UserInfoController {

    @Autowired
    private UserDao userDao;

    @ApiOperation(value = "Get user details", httpMethod = "GET")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Return user details with token exists"),
            @ApiResponse(code = 400, message = "Invalid request parameters")})
    @GetMapping("/v1/userInfo")
    public UserDto getUserDetails(final Principal principal) {
        String username = principal.getName();

        User user = getUserDao().findOne(username);
        List<TokenDto> tokens = new ArrayList<>();

        if (user != null) {
            for (UserToken userToken : user.getUserTokens()) {
                TokenDto tokenDto = new TokenDto();
                tokenDto.setLink("/" + userToken.getTokenIssuer().name().toLowerCase());
                tokenDto.setTokenIssuerId(userToken.getTokenIssuerId());
                tokenDto.setName(userToken.getTokenIssuer().name());
                tokenDto.setTokenIssuer(userToken.getTokenIssuer().name());
                tokens.add(tokenDto);

            }
        }
        UserDto userDto = new UserDto(username);
        userDto.setTokens(tokens);
        return userDto;
    }

    private UserDao getUserDao() {
        return userDao;
    }

}
