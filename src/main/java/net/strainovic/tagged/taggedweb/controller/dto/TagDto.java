package net.strainovic.tagged.taggedweb.controller.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.strainovic.tagged.taggedweb.model.TokenIssuer;
import org.hibernate.validator.constraints.NotBlank;

import java.util.Collection;

@ApiModel
public class TagDto {

    @NotBlank
    @ApiModelProperty(required = true)
    private String fileId;

    @NotBlank
    @ApiModelProperty(required = true)
    private String id;

    @NotBlank
    @ApiModelProperty
    private String name;

    @ApiModelProperty
    private TokenIssuer provider;

    @ApiModelProperty
    private String providerId;

    @ApiModelProperty(required = true)
    private Collection<String> tags;

    public TagDto(String id, String fileId, TokenIssuer provider, String providerId, Collection<String> tags) {
        this.id = id;
        this.fileId = fileId;
        this.provider = provider;
        this.providerId = providerId;
        this.tags = tags;
    }

    public TagDto() {
    }

    public String getFileId() {
        return fileId;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public TokenIssuer getProvider() {
        return provider;
    }

    public String getProviderId() {
        return providerId;
    }

    public Collection<String> getTags() {
        return tags;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setProvider(TokenIssuer provider) {
        this.provider = provider;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public void setTags(Collection<String> tags) {
        this.tags = tags;
    }
}
