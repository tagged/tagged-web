package net.strainovic.tagged.taggedweb.controller.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class ServiceDto {

    public ServiceDto(String name, String url) {
        this.name = name;
        this.url = url;
    }

    @ApiModelProperty
    private String name;

    @ApiModelProperty
    private String url;

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
