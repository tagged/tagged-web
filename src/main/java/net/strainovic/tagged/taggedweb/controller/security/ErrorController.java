package net.strainovic.tagged.taggedweb.controller.security;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Enumeration;

@SuppressWarnings("unused")
@Controller
public class ErrorController implements org.springframework.boot.autoconfigure.web.ErrorController {
    private static final String PATH = "/error";

    @RequestMapping("/error")
    protected String error(final RedirectAttributes redirectAttributes, final HttpServletRequest req) throws IOException {
        String referrer = req.getHeader("Referer");
        req.getSession().setAttribute("url_prior_login", referrer);

        Enumeration<String> e = req.getHeaderNames();

        while (e.hasMoreElements()) {
            String param = e.nextElement();
        }

        redirectAttributes.addFlashAttribute("error", true);
        return "redirect:/login";
    }

    @Override
    public String getErrorPath() {
        return PATH;
    }

}