package net.strainovic.tagged.taggedweb.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import net.strainovic.tagged.taggedweb.controller.command.AddTagCommand;
import net.strainovic.tagged.taggedweb.controller.command.EditTagCommand;
import net.strainovic.tagged.taggedweb.controller.command.FolderCommand;
import net.strainovic.tagged.taggedweb.controller.command.SearchCommand;
import net.strainovic.tagged.taggedweb.controller.dto.FileDto;
import net.strainovic.tagged.taggedweb.controller.dto.TagDto;
import net.strainovic.tagged.taggedweb.controller.dto.TokenDto;
import net.strainovic.tagged.taggedweb.controller.dto.UserDto;
import net.strainovic.tagged.taggedweb.model.TokenIssuer;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * This is dummy controller only for UI development. Remove this when UI is finished
 */
@Api(value = "Dummy", tags = "Dummy")
@RestController
public class DummyController {

    private static FileDto file1 = new FileDto("id1", "fileId1", TokenIssuer.DROPBOX, "randomId",
            false, new Date(), "Some file 1", "/", Arrays.asList("beach", "bar"));
    private static FileDto file2 = new FileDto("id2", "fileId2", TokenIssuer.DROPBOX, "randomId",
            false, new Date(), "Some file 2", "/", Collections.singletonList("car"));
    private static FileDto folder1 = new FileDto("id3", "fileId3", TokenIssuer.DROPBOX, "randomId",
            false, new Date(), "Folder1", "/", null);
    private static FileDto file3 = new FileDto("id4", "fileId4", TokenIssuer.DROPBOX, "randomId",
            false, new Date(), "Some file 3 from folder 1", "/folder1",
            Collections.singletonList("beach"));

    private static List<FileDto> FILES = new ArrayList<>(Arrays.asList(file1, file2, file3, folder1));

    @ApiOperation(value = "Folder search", httpMethod = "POST")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Folder search"),
            @ApiResponse(code = 400, message = "Invalid request parameters")})
    @PostMapping("/v2/folder")
    public List<FileDto> folder(@RequestBody @NotNull @Valid FolderCommand folderCommand) {
        return FILES.stream().filter(f -> f.getPath().equals(folderCommand.getPath())).collect(Collectors.toList());
    }

    @ApiOperation(value = "Search", httpMethod = "POST")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Search"),
            @ApiResponse(code = 400, message = "Invalid request parameters")})
    @PostMapping("/v2/search")
    public List<FileDto> search(@RequestBody @NotNull @Valid SearchCommand searchCommand) {
        List<FileDto> files = new ArrayList<>();

        for (String tag : searchCommand.getTags()) {
            List<FileDto> collected = FILES.stream()
                    .filter(f -> CollectionUtils.isNotEmpty(f.getTags()) && f.getTags().contains(tag))
                    .collect(Collectors.toList());
            files.addAll(collected);
        }

        return files.stream().distinct().collect(Collectors.toList());
    }

    @ApiOperation(value = "Add tags for specific document", httpMethod = "POST")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Provided tags are saved for document"),
            @ApiResponse(code = 400, message = "Invalid request parameters")})
    @PostMapping("/v2/tags")
    public TagDto addTag(@RequestBody @NotNull @Valid AddTagCommand addTagCommand) {
        Random r = new Random();

        String id = UUID.randomUUID().toString();
        String fileId = UUID.randomUUID().toString();

        FileDto fileDto = new FileDto(id, fileId, addTagCommand.getProvider(), addTagCommand.getProviderId(),
                false, new Date(), "Some name " + r.nextInt(100), "/", addTagCommand.getTags());
        FILES.add(fileDto);

        return new TagDto(id, fileId, addTagCommand.getProvider(), addTagCommand.getProviderId(), addTagCommand.getTags());
    }

    @ApiOperation(value = "Edit tags for specific document", httpMethod = "DELETE")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "All tags are deleted for document"),
            @ApiResponse(code = 400, message = "Invalid request parameters")})
    @DeleteMapping("/v2/tags/{id}")
    public void deleteTag(@PathVariable("id") String id) {
        Optional<FileDto> optionalFile = FILES.stream().filter(t -> t.getId().equals(id)).findFirst();
        optionalFile.ifPresent(fileDto -> FILES.remove(fileDto));
    }

    @ApiOperation(value = "Edit tags for specific document", httpMethod = "PUT")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Provided tags are saved for document"),
            @ApiResponse(code = 400, message = "Invalid request parameters")})
    @PutMapping("/v2/tags")
    public FileDto editTag(@RequestBody @NotNull @Valid EditTagCommand editTagCommand) {
        Optional<FileDto> optionalFile = FILES.stream()
                .filter(f -> f.getId().equals(editTagCommand.getId())).findFirst();

        if (optionalFile.isPresent()) {
            FileDto fileFromDb = optionalFile.get();
            fileFromDb.setTags(editTagCommand.getTags());
            return fileFromDb;
        }
        return null;
    }


    @ApiOperation(value = "Get user details", httpMethod = "GET")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Return user details with token exists"),
            @ApiResponse(code = 400, message = "Invalid request parameters")})
    @GetMapping("/v2/userInfo")
    public UserDto getUserDetails(final Principal principal) {
        UserDto userDto = new UserDto(principal.getName());
        List<TokenDto> tokens = new ArrayList<>();

        TokenDto tokenDto = new TokenDto();
        tokenDto.setLink("/" + TokenIssuer.DROPBOX.name().toLowerCase());
        tokenDto.setTokenIssuerId("123456789");
        tokenDto.setName(TokenIssuer.DROPBOX.name());
        tokenDto.setTokenIssuer(TokenIssuer.DROPBOX.name());
        tokens.add(tokenDto);

        userDto.setTokens(tokens);

        return userDto;
    }


}
