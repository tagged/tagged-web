package net.strainovic.tagged.taggedweb.controller.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class TokenDto {

    @ApiModelProperty
    private String link;

    @ApiModelProperty
    private String name;

    @ApiModelProperty
    private String tokenIssuer;

    @ApiModelProperty
    private String tokenIssuerId;

    public String getLink() {
        return link;
    }

    public String getName() {
        return name;
    }

    public String getTokenIssuer() {
        return tokenIssuer;
    }

    public String getTokenIssuerId() {
        return tokenIssuerId;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTokenIssuer(String tokenIssuer) {
        this.tokenIssuer = tokenIssuer;
    }

    public void setTokenIssuerId(String tokenIssuerId) {
        this.tokenIssuerId = tokenIssuerId;
    }
}
