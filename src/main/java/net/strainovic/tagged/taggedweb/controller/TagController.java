package net.strainovic.tagged.taggedweb.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import net.strainovic.tagged.taggedweb.controller.command.AddTagCommand;
import net.strainovic.tagged.taggedweb.controller.command.EditTagCommand;
import net.strainovic.tagged.taggedweb.controller.dto.TagDto;
import net.strainovic.tagged.taggedweb.hystrix.command.TaggedRestAddTagsCommand;
import net.strainovic.tagged.taggedweb.hystrix.command.TaggedRestEditTagsCommand;
import net.strainovic.tagged.taggedweb.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.security.Principal;

@Api(value = "Tags", tags = "Tags")
@RestController
public class TagController {

    @Autowired
    private TagService tagService;

    @ApiOperation(value = "Add tags for specific document", httpMethod = "POST")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Provided tags are saved for document"),
            @ApiResponse(code = 400, message = "Invalid request parameters")})
    @PostMapping("/v1/tags")
    public TagDto addTag(@RequestBody @NotNull @Valid AddTagCommand addTagCommand, final Principal principal) {
        String username = principal.getName();

        TaggedRestAddTagsCommand command = new TaggedRestAddTagsCommand();
        command.setFileId(addTagCommand.getFileId());
        command.setTags(addTagCommand.getTags());
        command.setName(addTagCommand.getName());
        command.setUsername(username);
        command.setProvider(addTagCommand.getProvider().name());
        command.setProviderId(addTagCommand.getProviderId());

        return getTagService().addTags(command);
    }

    @ApiOperation(value = "Edit tags for specific document", httpMethod = "PUT")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Provided tags are saved for document"),
            @ApiResponse(code = 400, message = "Invalid request parameters")})
    @PutMapping("/v1/tags")
    public TagDto editTag(@RequestBody @NotNull @Valid EditTagCommand editTagCommand) {
        TaggedRestEditTagsCommand command = new TaggedRestEditTagsCommand();
        command.setId(editTagCommand.getId());
        command.setTags(editTagCommand.getTags());

        return getTagService().editTags(command);

    }

    @ApiOperation(value = "Edit tags for specific document", httpMethod = "DELETE")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "All tags are deleted for document"),
            @ApiResponse(code = 400, message = "Invalid request parameters")})
    @DeleteMapping("/v1/tags/{id}")
    public void deleteTag(@PathVariable("id") String id) {
        getTagService().deleteTags(id);
    }

    private TagService getTagService() {
        return tagService;
    }

}
