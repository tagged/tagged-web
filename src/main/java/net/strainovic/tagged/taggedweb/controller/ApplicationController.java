package net.strainovic.tagged.taggedweb.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import net.strainovic.tagged.taggedweb.controller.dto.ServiceDto;
import net.strainovic.tagged.taggedweb.model.TokenIssuer;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@Api(value = "Application", tags = "Application", description = "Application based data", consumes = "application/json")
@RestController
public class ApplicationController {

    @ApiOperation(value = "Get all available services for connection", httpMethod = "GET")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Return list of available services with url's"),
            @ApiResponse(code = 400, message = "Invalid request parameters")})
    @GetMapping("/v1/services")
    public List<ServiceDto> getAvailableServices() {
        ServiceDto dropboxService = new ServiceDto(TokenIssuer.DROPBOX.name(), "/v1/dropboxAuth");

        List<ServiceDto> services = new ArrayList<>();
        services.add(dropboxService);
        return services;
    }

}
