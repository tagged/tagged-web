package net.strainovic.tagged.taggedweb.service.impl;

import com.netflix.hystrix.HystrixExecutable;
import net.strainovic.tagged.taggedweb.controller.dto.FileDto;
import net.strainovic.tagged.taggedweb.dao.UserDao;
import net.strainovic.tagged.taggedweb.hystrix.HystrixCommandFactory;
import net.strainovic.tagged.taggedweb.model.TokenIssuer;
import net.strainovic.tagged.taggedweb.model.User;
import net.strainovic.tagged.taggedweb.service.FolderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class FolderServiceImpl implements FolderService {

    private final RestTemplate restTemplate;

    private final UserDao userDao;

    @Autowired
    public FolderServiceImpl(RestTemplate restTemplate, UserDao userDao) {
        this.restTemplate = restTemplate;
        this.userDao = userDao;
    }

    @Override
    public List<FileDto> folderSearch(String username, TokenIssuer provider, String providerId, String path) {
        RestTemplate restTemplate = getRestTemplate();
        User user = getUserDao().findOne(username);

        HystrixExecutable<List<FileDto>> hystrixExecutable = HystrixCommandFactory
                .getFolderCommand(user, provider, providerId, restTemplate, path);

        List<FileDto> files = hystrixExecutable.execute();
        files.forEach(f -> {
            f.setProvider(provider);
            f.setProviderId(providerId);
        });

        return files;
    }

    private RestTemplate getRestTemplate() {
        return restTemplate;
    }

    private UserDao getUserDao() {
        return userDao;
    }
}
