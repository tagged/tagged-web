package net.strainovic.tagged.taggedweb.service;

import net.strainovic.tagged.taggedweb.controller.dto.TagDto;
import net.strainovic.tagged.taggedweb.hystrix.command.TaggedRestEditTagsCommand;
import net.strainovic.tagged.taggedweb.hystrix.command.TaggedRestSearchCommand;
import net.strainovic.tagged.taggedweb.hystrix.command.TaggedRestAddTagsCommand;

import java.util.List;

public interface TagService {

    TagDto addTags(TaggedRestAddTagsCommand command);

    void deleteTags(String id);

    TagDto editTags(TaggedRestEditTagsCommand command);

    List<TagDto> search(TaggedRestSearchCommand command);
}
