package net.strainovic.tagged.taggedweb.service;

import net.strainovic.tagged.taggedweb.controller.dto.FileDto;
import net.strainovic.tagged.taggedweb.model.TokenIssuer;

import java.util.List;

public interface FolderService {

    List<FileDto> folderSearch(String username, TokenIssuer provider, String providerId, String path);
}
