package net.strainovic.tagged.taggedweb.service.impl;

import com.netflix.hystrix.HystrixExecutable;
import net.strainovic.tagged.taggedweb.controller.dto.TagDto;
import net.strainovic.tagged.taggedweb.hystrix.command.TaggedRestAddTagsCommand;
import net.strainovic.tagged.taggedweb.hystrix.command.TaggedRestEditTagsCommand;
import net.strainovic.tagged.taggedweb.hystrix.command.TaggedRestSearchCommand;
import net.strainovic.tagged.taggedweb.hystrix.tag.AddTagsHystrixCommand;
import net.strainovic.tagged.taggedweb.hystrix.tag.DeleteTagsHystrixCommand;
import net.strainovic.tagged.taggedweb.hystrix.tag.EditTagsHystrixCommand;
import net.strainovic.tagged.taggedweb.hystrix.tag.TagsHystrixCommand;
import net.strainovic.tagged.taggedweb.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class TagServiceImpl implements TagService {

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public TagDto addTags(TaggedRestAddTagsCommand command) {
        HystrixExecutable<TagDto> hystrixExecutable = new AddTagsHystrixCommand(getRestTemplate(), command);
        return hystrixExecutable.execute();
    }

    @Override
    public void deleteTags(String id) {
        HystrixExecutable hystrixExecutable = new DeleteTagsHystrixCommand(getRestTemplate(), id);
        hystrixExecutable.execute();
    }

    @Override
    public TagDto editTags(TaggedRestEditTagsCommand command) {
        HystrixExecutable<TagDto> hystrixExecutable = new EditTagsHystrixCommand(getRestTemplate(), command);
        return hystrixExecutable.execute();
    }

    @Override
    public List<TagDto> search(TaggedRestSearchCommand command) {
        HystrixExecutable<List<TagDto>> hystrixExecutable = new TagsHystrixCommand(getRestTemplate(), command, true);
        return hystrixExecutable.execute();
    }

    private RestTemplate getRestTemplate() {
        return restTemplate;
    }
}
